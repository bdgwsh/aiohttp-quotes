import aiohttp_jinja2
import jinja2
from aiohttp import web

from src.quotes import init_pg, close_pg
from src.quotes import setup_routes
from src.quotes import config

if __name__ == '__main__':
    app = web.Application()
    app['config'] = config
    aiohttp_jinja2.setup(
        app, loader=jinja2.PackageLoader('quotes', 'templates'))
    setup_routes(app)
    app.on_startup.append(init_pg)
    app.on_cleanup.append(close_pg)
    web.run_app(app, host=config['app']['host'], port=config['app']['port'])