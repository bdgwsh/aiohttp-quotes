from .db import quotes
import aiohttp_jinja2


@aiohttp_jinja2.template('index.html')
async def index(request):
    global quotes
    async with request.app['db'].acquire() as conn:
        cursor = await conn.execute(quotes.select())
        records = await cursor.fetchall()
        quotes = [dict(q) for q in records]
        return {'quotes': quotes}

