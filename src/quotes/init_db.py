from sqlalchemy import create_engine, MetaData

from .settings import config
from .db import quotes


DSN = "postgresql://{user}:{password}@{host}:{port}/{database}"


def create_tables(engine):
    meta = MetaData()
    meta.create_all(bind=engine, tables=[quotes])


def sample_data(engine):
    conn = engine.connect()
    conn.execute(quotes.insert(), [
        {
             'text': 'Talent hits a target no one else can hit. Genius hits a target no one else can see.',
             'author': 'Arthur Schopenhauer',
        },
        {
            'text': '....',
            'author': 'Gordon Freeman',
        },
        {
            'text': 'Learning never exhausts the mind.',
            'author': 'Leonardo da Vinci'
        },
        {
            'text': 'Let us sacrifice our today so that our children can have a better tomorrow.',
            'author': 'A. P. J. Abdul Kalam'
        },
        {
            'text': 'But man is not made for defeat. A man can be destroyed but not defeated.',
            'author': 'Ernest Hemingway'
        }
    ])
    conn.close()


if __name__ == '__main__':
    db_url = DSN.format(**config['postgres'])
    engine = create_engine(db_url)

    create_tables(engine)
    sample_data(engine)