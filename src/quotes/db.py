
from datetime import datetime as dt
import aiopg
from aiopg.sa import create_engine

from sqlalchemy import (
    MetaData, Table, Column,
    Integer, String, Date, DateTime
)

meta = MetaData()

quotes = Table(
    'quotes', meta,

    Column('id', Integer, primary_key=True),
    Column('text', String(200), nullable=False),
    Column('author', String(200), nullable=False, default='anonymous'),
    Column('timestamp', DateTime, index=True, default=dt.utcnow),
)


async def init_pg(app):
    conf = app['config']['postgres']
    engine = await create_engine(
        database=conf['database'],
        user=conf['user'],
        password=conf['password'],
        host=conf['host'],
        port=conf['port'],
        minsize=conf['minsize'],
        maxsize=conf['maxsize'],
    )
    app['db'] = engine


async def close_pg(app):
    app['db'].close()
    await app['db'].wait_closed()
