## Quotes Web App based on Aiohttp
This is my very simple learning project that is focused on creating web app using
*aiohttp*.
<img src="screens/index.png" alt="index_page" />

## Running
1. Firstly install dependencies.
* ```console
  $ pip install -r requirements.txt
  ```

2. Create database and postgres user.

- ```console
  $ sudo -su postgres psql
  ```

- ```console
  $psql: CREATE USER quotes with password quotes;
  ```

- ```console
  $psql: GRANT ALL ON DATABASE quotes to quotes;
  ```

- ```console
  $psql: CREATE DATABASE quotes;
  ```

3. Go to `src/quotes` and type
    ```console
    $ python init_db.py
    ```
   to create tables and populate db.
4. Run it by typing:
    ```console
    $ python main.py
    ```